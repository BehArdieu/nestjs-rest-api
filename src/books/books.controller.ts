import { Controller, Get } from '@nestjs/common';

type Book = {
  title: string;
  author: string;
};

@Controller('books')
export class BooksController {
  private books: Book[] = [
    {
      title: 'Les frères Karamazov',
      author: 'Fiodor Dostoïevski',
    },
    {
      title: "L'étranger",
      author: 'Albert Camus',
    },
  ];

  // GET http://localhost:3000/books
  @Get()
  findAll() {
    return this.books;
  }
}
